# This is a hybrid between:
#   https://github.com/ohbarye/rails-react-typescript-docker-example
#   https://docs.docker.com/compose/rails/
#   https://medium.com/greedygame-engineering/so-you-want-to-dockerize-your-react-app-64fbbb74c217


ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

.DEFAULT_GOAL := help

# Make this the default command 
help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
  
# `make setup` will be used after cloning or downloading to fulfill
# dependencies, and setup the the project in an initial state.
# This is where you might download rubygems, node_modules, packages,
# compile code, build container images, initialize a database,
# anything else that needs to happen before your server is started
# for the first time

setup:		## Sets up the docker files.
	docker-compose build backend
	docker-compose build frontend
	docker-compose run backend bundle exec rake db:create
	docker-compose run backend bundle exec rake db:migrate
	

# `make server` will be used after `make setup` in order to start
# an http server process that listens on any unreserved port
#	of your choice (e.g. 8080). 

server:		## After running `make setup` this turns the servers on..
	# Go to http://localhost (nginx serves on port 80)
	docker-compose up -d
	@echo "    - servers started, go to http://localhost"
	@open "http://localhost"

# `make test` will be used after `make setup` in order to run
# your test suite.

test:		## Test the apps and lists where coverage reports exists at.
	# Because we do not create & expose coverage in the
	#   docker builds
	# So install dependencies
	cd backend && gem install bundler && bundle install && rails test	
	cd frontend && yarn && yarn test
	# Echo where the files could be located
	@echo "Fing rails coverage report at $(ROOT_DIR)/backend/coverage/index.html"
	@echo "Fing reactJS coverage report at $(ROOT_DIR)/frontend/coverage/index.html"
	# Try to open the coverage reports
	@open $(ROOT_DIR)/backend/coverage/index.html
	@open $(ROOT_DIR)/frontend/coverage/index.html


stop:		## Turns servers off.
	docker-compose down

remove:		## Removes the docker images created by this file
    # PIN: https://vsupalov.com/cleaning-up-after-docker/
	docker-compose down -v --rmi all --remove-orphans
	docker-compose rm -fsv
    # docker-compose down -v --rmi all --remove-orphans