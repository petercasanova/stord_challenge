require "test_helper"

class ShortenerTest < ActiveSupport::TestCase

  # Most of these are integration test because
  #   we want the model to call the before_create method
  def setup
    @validShortener = Shortener.new(url: "http://msn.com")
  end

  test "valid shortener" do
    assert @validShortener.valid?
  end

  test "prevent duplicate URL values" do
    @validShortener.save()
    assert Shortener.new(url: "http://msn.com").invalid?
  end

  test "creates a short url" do
    @validShortener.save()
    assert @validShortener.short_url.present?
  end

  test "prevents invalid URL" do
    assert Shortener.new(url: "invalid string").invalid?
  end

  # What is not tested from app/models/shortener.rb:
  #   * Lines 55-56 > While loop where it checks if generated string exists in DB with count
  #     PROBLEM: method creates a random string and stores in DB.  Can't think of a way
  #            to pre-populate the DB with the string it will generate.
  #   * Line 63 > Raises an exemption if 10+ tries have happened in the loop above
  #     PROBLEM: Same as above
  #   * Line 69 > Warn when 5+ attempts have been tried
  #     PROBLEM: Same as above
end
