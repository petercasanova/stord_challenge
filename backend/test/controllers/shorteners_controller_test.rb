require "test_helper"

class ShortenersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @shortener = shorteners(:valid)
  end

  test "should get index" do
    get shorteners_url, as: :json
    assert_response :success
  end

  test "should create shortener" do
    assert_difference("Shortener.count") do
      post shorteners_url, params: { shortener: { url: "http://ShortenersControllerTest.com" } }, as: :json
    end

    assert_response 201
  end

  test "should show shortener" do
    get shortener_url(@shortener), as: :json
    assert_response :success
  end

  test "can find by short_url" do
    get "/shorteners/#{@shortener.short_url}", as: :json
    assert_response :success
  end

  test "returns existing if already exists" do
    post shorteners_url, params: { shortener: { url: @shortener.url } }, as: :json
    assert_response 201
  end

  test "returns error if json is invalid" do
    # NOTE, { url: @shortener.url } is valid
    # So not only remove the { shortener: {...} } but
    #  also change the attribute
    post shorteners_url, params: { bad: @shortener.url }, as: :json
    assert_response 400
  end

  test "should error when a bad URL is sent" do
    post shorteners_url, params: { shortener: { url: "foobar" } }, as: :json
    assert_response 422
  end
end
