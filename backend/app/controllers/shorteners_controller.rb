class ShortenersController < ApplicationController
  before_action :set_shortener, only: [:show]

  # GET /shorteners
  def index
    @shorteners = Shortener.all

    render json: @shorteners
  end

  # GET /shorteners/short_url
  def show
    render json: @shortener
  end

  # POST /shorteners
  def create
    #  Be nice, if URL already exists
    #  Just return what we have
    @shortener = Shortener.find_or_create_by(shortener_params)

    if @shortener.save
      render json: @shortener, status: :created, location: @shortener
    else
      render json: @shortener.errors, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_shortener
    @shortener = Shortener.where(short_url: params[:short_url])
  end

  # Only allow a trusted parameter "white list" through.
  def shortener_params
    params.require(:shortener).permit(:url)
  end
end
