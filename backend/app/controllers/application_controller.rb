class ApplicationController < ActionController::API

  # Respong with JSON error
  # It has been a while since I used Rails, not sure if this
  #   is best practice or not.
  rescue_from ActionController::ParameterMissing do |e|
    render json: { error: e.message }, status: :bad_request
  end
end
