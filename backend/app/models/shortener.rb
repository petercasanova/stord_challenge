# This model is the core of the application.
# Takes in a unique URL then converts it to a short url
# based on the "generate_random_string" method.
# The short_url attribute is what is used on the GET request.
# short_url is currently based on a subset of unique
# alphanumeric characters plus the underscore.  Alphanumeric
# values are both upper and lower case.

# NOTE: We only expose the CREATE method NOT the UPDATE!

class Shortener < ApplicationRecord

  # Using the https://github.com/perfectline/validates_url gem
  # Creating a URL parser / validator tends to be complex with
  #   edge cases.  For now, unload this issue to the GEM due to
  #   time constraints.
  validates :url, uniqueness: true, presence: :true, url: true
  validates :short_url, uniqueness: true

  # We only take the URL on create, this triggers the creation
  #   of the short_url
  before_create :generate_short_url

  # 63 unique characters
  # All the alphabets both lower case and uppercase,
  #   all numbers between 0 - 9 and underscore
  STRING_ARRAY = [*"a".."z", *"A".."Z", *"0".."9", "_"]

  # Throw a little chaos into the mix
  STRING_LENGTH_ARRAY = [*"5".."10"]

  # Instead of using ID on the GET request
  # use the short_url.
  # Could have over-written in the controller but
  #   this could confuse other developers with what
  #   attribute is being used in the FIND.
  def to_param
    short_url
  end

  # These methods have no purpose as public methods on the model.
  private

  # What creates the short_url for a model
  def generate_short_url
    # We could put this in a 1 liner vs a seperate method
    #   but this should really exist somewhere else so mock it as such.
    #   i.e. lib, module, etc.
    short_url = generate_random_string

    # Gaurd against an infinite loop
    # For now, limit to 10 trys
    count = 0
    while exist = Shortener.where(short_url: short_url).exists? && count < 10
      short_url = generate_random_string
      count += 1
    end

    # This method is no longer useful.  Time to try something else.
    # Instead of saving bad data, just crash and fix.
    # Using block instead of 1 liner to limit the character width of file.
    if exist && count >= 10
      raise "Our 'generate_short_url' method is beginning to fail"
    end

    # Start warning that method is starting to fail.
    # Using block instead of 1 liner to limit the character width of file.
    if (count >= 5)
      Rails.logger.warn("'generate_short_url' is starting to fail, tried #{count} times")
    end
    self.short_url = short_url
  end

  def generate_random_string
    # Besides plucking N number of characters from the array
    #  also randomize the length of characters being plucked.
    # This serves 2 purposes
    #   - shorter is sweeter, so try to create as short as possible
    #   - at the end, this is a finite number of possible unique
    #       values, so by randomizing the number of characters
    #       being plucked, it extends the life of this method a
    #       little more.
    STRING_ARRAY.sample(STRING_LENGTH_ARRAY.sample.to_i).join()
  end
end
