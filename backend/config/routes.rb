Rails.application.routes.draw do
  resources :shorteners, only: [:index, :show, :create], param: :short_url, defaults: { format: :json }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
