class CreateShorteners < ActiveRecord::Migration[5.2]
  def change
    create_table :shorteners do |t|
      t.string :url, null: false
      t.string :short_url, null: false

      t.timestamps
    end
    add_index :shorteners, :url, unique: true
    add_index :shorteners, :short_url, unique: true
  end
end
