# README

### Purpose of this app

Base API to create a URL shortener.  

## Running the application for the first time in development mode

* Download and cd into the root application directory.
* Open terminal console
* Install ruby 2.6.5 `rvm install 2.6.5`
* App uses bundler 2+
  * If you get a bundler error run `gem update bundler`
* Install gems `bundle install`
* Create database `bundle exec rake db:create`
  * Using SQLLite3
* Run migrations `bundle exec rake db:migrate`


## Running the server

* From console `bundle exec rails s`

## Running tests

* From console `bundle exec rake test`
* Coverage report is then available at `coverage/index.html`

## Additional info

App has the (Rufo gem)[https://github.com/ruby-formatter/rufo], if you use Visual Studo code, there is a plugin for it https://marketplace.visualstudio.com/items?itemName=mbessey.vscode-rufo
