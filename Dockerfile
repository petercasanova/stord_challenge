FROM ruby:2.6.5-slim

WORKDIR /myapp/backend/rails-app

# RUN apt-get update -qq && apt-get install -y build-essential

RUN apt-get update -qq 

# Need build tools
RUN apt-get install -y --no-install-recommends build-essential apt-utils


# for a sqllite3 
RUN apt-get install -y sqlite3 libsqlite3-dev

# for a JS runtime
RUN apt-get install -y nodejs

COPY backend /myapp/backend/rails-app
# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
# EXPOSE 3000

RUN gem install bundler
RUN bundle install

# Clean up after build
RUN apt-get purge -y --auto-remove build-essential apt-utils

# Start the main process.
# CMD ["rails", "server", "-b", "0.0.0.0"]