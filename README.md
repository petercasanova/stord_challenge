# STORD URL Shortener Exercise


## Deliverable

- Fork or clone this repository
- Makefile is based on *nix machines.
- Additional information could be found in the "notes.txt" in the root directory.

## First time run

From terminal window run `make setup`

This should create the docker images needed for the environments.  This process takes time, so be patient please.

## Starting the servers

From terminal window run `make server`

This will run the docker-compose file.  After servers are started, your browser should open displaying the app.

*One of the servers runs on port 3000, so please stop any local servers that use this port.*


## Should Make fail

You will need to install Ruby 2.6.5, bundler 2+ and Yarn.

Then from Termainal 1, cd to ROOT_REPO/backend and run, from the command line:

- `bundle install`
- `bundle exec rake db:create && rake db:migrate`
- `bundle exec rails s`

Then from Termainal 2, cd to ROOT_REPO/backend and run, from the command line: 

- `yarn install`
- `yarn start`

Finally go to `http://localhost:8080`

Bothe the "backend" and "frontend" directories have their own README.