const HtmlWebPackPlugin = require("html-webpack-plugin");
const babelOptions = require("./babel.config");

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        enforce: "pre",
        loader: "eslint-loader",
      },
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: babelOptions,
        },
      },
      { test: /\.css$/, use: ["style-loader", "css-loader", "postcss-loader"] },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/",
              mimetype: "application/font-woff",
              publicPath: url => `/fonts/${url}`,
            },
          },
        ],
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
    hot: true,
    publicPath: "/",
    inline: true,
    overlay: true,
    contentBase: "dist",
  },
  performance: {
    maxEntrypointSize: 1024000, // we're just a hair over the defaults
    maxAssetSize: 1024000, // In real prod, this is split into chunks with optimize
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html",
    }),
  ],
};
