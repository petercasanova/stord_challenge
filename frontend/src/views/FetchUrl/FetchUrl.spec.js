import React from "react";
import { cleanup, waitForElement } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { act } from "react-dom/test-utils";

// Import test setup scripts
import axiosMock from "axios";
import RenderWithRouter from "../../../tests/RenderWithRouter";

import FetchUrl from "./FetchUrl";

afterEach(cleanup);

describe("FetchUrl", () => {
  it("with valid URL redirects correctly", async () => {
    // Items shared within the test
    const url = "another_url";
    let linearProgressDiv;

    // Don't actually redirect, but look for the function call
    const replacer = jest.fn();
    window.location.replace = replacer;

    // Mock the Axios Request
    axiosMock.get.mockResolvedValue({ data: [{ url }] });

    // Create the component
    const { getByTestId } = RenderWithRouter(<FetchUrl />, { route: "my_route" });

    // Wait for some initial states
    await waitForElement(() => {
      return (linearProgressDiv = getByTestId("linear-progress"));
    });

    // Make sure Progress Bar is showing
    expect(linearProgressDiv).toBeVisible();

    // The Redirect
    act(() => {
      expect(replacer).toHaveBeenCalledWith(url);
    });
  });

  it("with invalid URL shows error", async () => {
    // Items shared within the test
    const url = "another_url";
    let linearProgressDiv;
    let errorDiv;

    // Mock the Axios Request
    axiosMock.get.mockRejectedValue({
      status: 404,
      data: { message: "Not Found" },
    });

    // Create the component
    const { getByTestId } = RenderWithRouter(<FetchUrl />, { route: "bad_route" });

    // Wait for some initial states
    await waitForElement(() => {
      return (linearProgressDiv = getByTestId("linear-progress"));
    });

    // Make sure Progress Bar is showing
    expect(linearProgressDiv).toBeVisible();

    // After the Request fails
    await waitForElement(() => {
      return (errorDiv = getByTestId("error"));
    });

    // Make sure the Error Div is showing
    expect(errorDiv).toBeVisible();
  });
});
