import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import { Link, useParams } from "react-router-dom";

import axios from "axios";

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh",
    minHeight: "100px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  urlWrapper: {
    flexDirection: "column",
    minWidth: "300px",
    textAlign: "center",
  },
  link: {
    fontSize: "1.2rem",
    color: theme.palette.primary.main,
    textDecoration: "none",
  },
}));

export default function FetchUrl() {
  const classes = useStyles();
  let { url } = useParams();
  const [fetching, setFetching] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    axios
      .get(`http://localhost:3000/shorteners/${url}`)
      .then(({ data }) => {
        setFetching(false);
        window.location.replace(data[0].url);
      })
      .catch(err => {
        setFetching(false);
        setError(true);
        // console.log("---- error: ", err);
      });
  }, [url]);
  return (
    <div className={classes.root}>
      {fetching && (
        <div className={classes.urlWrapper}>
          <LinearProgress data-testid="linear-progress" className={classes.progress} />
        </div>
      )}

      {error && (
        <div data-testid="error" className={classes.urlWrapper}>
          <Typography>
            There seems to have been a problem with your URL <b>{url}</b>.
          </Typography>
          <Link className={classes.link} to="/">
            Would you like to try creating one?
          </Link>
        </div>
      )}
    </div>
  );
}
