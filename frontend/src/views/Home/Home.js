import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";

import DisplayShortUrl from "../../components/DisplayShortUrl/DisplayShortUrl";

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh",
    minHeight: "100px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  form: {
    "& > *": {
      margin: theme.spacing(1),
      width: 200,
    },
  },
}));

export default function BaseForm() {
  const classes = useStyles();
  const [url, setUrl] = useState("");
  const [submiting, setSubmiting] = useState(false);
  const [error, setError] = useState(false);
  const [shortUrl, setShortUrl] = useState(undefined);

  const handleSubmit = event => {
    event.preventDefault();
    setSubmiting(true);
    setError(false);
    axios
      .post("http://localhost:3000/shorteners/", { url })
      .then(resp => {
        setSubmiting(false);
        setShortUrl(resp.data.short_url);
      })
      .catch(err => {
        setSubmiting(false);
        // console.log("---- error:", err.response);
        let msg = "";
        switch (err.response.status) {
          case 422:
            msg = `URL: ${err.response.data.url.join(", ")}`;
            break;
          default:
            msg = "Server is mis behaving.";
            break;
        }
        setError(msg);
      });
  };

  const clearUrl = () => {
    setUrl("");
    setShortUrl(undefined);
  };

  return (
    <div className={classes.root}>
      {!shortUrl && (
        <form className={classes.form} noValidate autoComplete="off" onSubmit={handleSubmit}>
          <TextField
            id="url-field"
            inputProps={{
              "data-testid": "url-field",
            }}
            label="Feed me a URL"
            autoFocus
            defaultValue={url}
            onChange={evt => setUrl(evt.target.value.trim())}
            error={error !== false}
            helperText={error !== false ? error : ""}
          />
          <Button
            data-testid="submit-button"
            disabled={submiting === true}
            variant="contained"
            color="primary"
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </form>
      )}
      {shortUrl !== undefined && <DisplayShortUrl clearUrl={clearUrl} url={shortUrl} />}
    </div>
  );
}
