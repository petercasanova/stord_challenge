import React from "react";
import { cleanup, waitForElement, fireEvent } from "@testing-library/react";
import { shallow } from "enzyme";
import "@testing-library/jest-dom/extend-expect";

// Import test setup scripts
import axiosMock from "axios";
import RenderWithRouter from "../../../tests/RenderWithRouter";

import Home from "./Home";

afterEach(cleanup);

describe("Home", () => {
  it("renders correctly", () => {
    // Create the component
    const { container } = RenderWithRouter(<Home />);

    expect(container).toMatchSnapshot();
  });

  it("can submit successfully", async () => {
    const short_url = "server_response_short_url";
    axiosMock.post.mockResolvedValue({ data: { short_url } });
    const { getByTestId, getByText } = RenderWithRouter(<Home />);
    fireEvent.change(getByTestId("url-field"), {
      target: { value: "My_Url" },
    });

    fireEvent.click(getByTestId("submit-button"));
    let shortUrlText;
    await waitForElement(() => {
      return (shortUrlText = getByText(short_url));
    });
    expect(shortUrlText).toBeInTheDocument();

    // Now clear and reset the form
    fireEvent.click(getByTestId("reset-link"));
    let resetInput;
    await waitForElement(() => {
      return (resetInput = getByTestId("url-field"));
    });
    expect(resetInput.value).toBe("");
  });

  it("can render 422 error", async () => {
    const error_msg = "I am the error message";
    axiosMock.post.mockRejectedValue({ response: { status: 422, data: { url: [error_msg] } } });
    const { getByTestId, getByText } = RenderWithRouter(<Home />);
    fireEvent.change(getByTestId("url-field"), {
      target: { value: "My_bad_Url" },
    });

    fireEvent.click(getByTestId("submit-button"));
    let errorText;
    await waitForElement(() => {
      return (errorText = getByText(new RegExp(error_msg)));
    });
    expect(errorText).toBeInTheDocument();
  });

  it("can renders other errors", async () => {
    const error_msg = "some_random_error";
    const defaultErrorMsg = "Server is mis behaving";
    axiosMock.post.mockRejectedValue({ response: { status: 500, data: { url: [error_msg] } } });
    const { getByTestId, getByText } = RenderWithRouter(<Home />);
    fireEvent.change(getByTestId("url-field"), {
      target: { value: "My_bad_Url" },
    });

    fireEvent.click(getByTestId("submit-button"));
    let errorText;
    await waitForElement(() => {
      return (errorText = getByText(new RegExp(defaultErrorMsg)));
    });
    expect(errorText).toBeInTheDocument();
  });
});
