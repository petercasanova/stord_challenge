import React from "react";
import { shallow } from "enzyme";
import { cleanup, render, waitForElement, fireEvent } from "@testing-library/react";

import RenderWithRouter from "../../../tests/RenderWithRouter";
import DisplayShortUrl from "./DisplayShortUrl";

afterEach(cleanup);

describe("BaseForm", () => {
  let wrapper;
  let props = { url: "dummy_short_url" };

  it("renders correctly", () => {
    wrapper = shallow(<DisplayShortUrl {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("calls the clear function on reset", () => {
    const func = jest.fn();
    props.clearUrl = func;
    const { container, getByTestId } = RenderWithRouter(<DisplayShortUrl {...props} />);
    fireEvent.click(getByTestId("reset-link"));
    expect(func).toHaveBeenCalled();
  });
});
