import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh",
    minHeight: "100px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  urlWrapper: {
    flexDirection: "column",
    minWidth: "300px",
    textAlign: "center",
  },
  link: {
    fontSize: "1.2rem",
    color: theme.palette.primary.main,
    textDecoration: "none",
  },
  resetLink: {
    display: "block",
    fontStyle: "italic",
    marginTop: "1rem",
    color: theme.palette.primary.main,
    "&:hover": {
      cursor: "pointer",
    },
  },
}));

export default function DisplayShortUrl({ url, clearUrl }) {
  const classes = useStyles();
  const resetUrl = () => {
    clearUrl();
  };

  return (
    <div className={classes.root}>
      <div className={classes.urlWrapper}>
        <Typography>Would you like to test it? </Typography>
        <Link className={classes.link} to={`/${url}`}>
          {url}
        </Link>
        <Typography data-testid="reset-link" className={classes.resetLink} onClick={resetUrl}>
          Want to do another, click here.
        </Typography>
      </div>
    </div>
  );
}
