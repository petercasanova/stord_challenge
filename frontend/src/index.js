import React from "react";
import ReactDOM from "react-dom";

import "typeface-roboto/index.css";

import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme, responsiveFontSizes, ThemeProvider } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";

import Home from "./views/Home/Home";
import FetchUrl from "./views/FetchUrl/FetchUrl";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

let theme = createMuiTheme({
  palette: {
    primary: purple,
    type: "light",
  },
});
theme = responsiveFontSizes(theme);

const App = () => {
  return (
    <div>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path="/:url">
              <FetchUrl />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </ThemeProvider>
    </div>
  );
};

ReactDOM.render(<App />, document.querySelector("#root"));
