# README

### Purpose of this app

Base UI to create a URL shortener.  

## Running the application for the first time in development mode

* Download and cd into the root application directory.
* Open terminal console
* Install npm modules `yarn install`

## Running the server

* From console `yarn start`

## Running tests

* From console `yarn test`
* Coverage report is then available at `coverage/index.html`